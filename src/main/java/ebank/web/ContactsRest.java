package ebank.web;

import java.net.URI;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


import ebank.contacts.Contact;
import ebank.contacts.ContactRepository;

@Path("/contacts")
@Stateless
public class ContactsRest {

	@EJB
	private ContactRepository contactRepository;
	
    @Context
    private UriInfo uriInfo;
	
    @GET
    @Path("/")
    public Response findAllContacts() {
    	System.out.println("GET findAllContacts");
    	
    	List<Contact> contacts = contactRepository.findAll();
    	
    	System.out.println("contacts = " + contacts);

        return Response.ok(contacts, MediaType.APPLICATION_JSON).build();
    }
    
    @GET
    @Path("/{id}")
    public Response find(@PathParam("id") Long id) {
    	System.out.println("GET /" + id);
    	
    	Contact c = contactRepository.findById(id);

        return Response.ok(c, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Contact contact) {
        Contact c = contactRepository.add(contact);
        System.out.println("POST /" + c);

        return Response.ok(c, MediaType.APPLICATION_JSON).build();

    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(Contact contact) {
    	Contact c = contactRepository.update(contact);
    	System.out.println("PUT /" + c);
        return Response.ok(c, MediaType.APPLICATION_JSON).build();

    }

    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") Long itemId) {
    	contactRepository.remove(itemId);
        return Response.noContent().build();
    }
}
