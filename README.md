# Ebank
*Ebank* is an online banking system built on;
 - EJB
 - TomEE
 - JAXRS
 - JPA
 - AngularJS
 - Maven
 - Hibernate
 - MySQL
 
The project is built as part of the J2EE course Computer Eng, FET - UB @ 2016/17

## Requirements
The following are required to build and run this program
 - [Maven](https://maven.apache.org/download.cgi)
 - [MySQL](https://www.mysql.com/downloads/)
 - Internet access - *For first time run*
 
## Setup
Setup a MySQL database:
```
$ mysql -u<root_user> -p
$ enter password: ********

mysql> CREATE DATABASE ebank; 
mysql> CREATE USER 'ebank'@'localhost' IDENTIFIED BY 'ebank';
mysql> GRANT ALL PRIVILEGES ON ebank.* TO 'ebank'@'localhost';
```

## Build and Run
### Build
Clean working dir and package.  
`mvn clean package`

### Run
Use tomee plugin  
`mvn tomee:run`

### Build and Run all together
If you don't have time to run to commands;  
`mvn clean package tomee:run`  


open your browser/rest client at:
`http://localhost:8080/ebank/api/<endpoint>`

## Swagger
Swagger json available at: `http://localhost:8080/ebank/rest/api-docs/`  
Swagger UI available at: `http://localhost:8080/ebank/swagger-ui/`

## Contribution
Your contributions are welcomed.  
Before contributing, go throug the [contribution guidelines](#/CONTRIBUTION.md)

## Apidocs
Documentation for the api is found in APIDOCS.md


## Maintainer
This project is lead and maintained by Acha Bill.  
Github: [achabill](https://github.com/achabill)