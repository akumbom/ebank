# Documentation for the API

 | Endpoint              | method   | description               |   return code |
 | -------------         |:--------:| ------------             :| :---------------
 | /api/contacts/        | GET      | Get all contacts          | OK
 | /api/contacts/{id}    | GET      | Get contact by id         | OK
 | /api/contacts/        | POST     | Add contact               | CREATED
 | /api/contacts/        | PUT      | Update contact            | OK
 | /api/contacts/{id}    | DELETE   | Delete contact by id      | -



